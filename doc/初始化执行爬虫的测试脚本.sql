﻿USE `spider`;


--测试sql 爬java资料 
insert  into `spider_site`(`site_id`,`type`,`name`,`url`,`domain`,`is_enable`) values (1,10,'java入门资料','http://www.runoob.com/java/','http://www.runoob.com/java/',1);
insert  into `spider_rule`(`rule_id`,`regex`,`title_select`,`content_select`,`site_id`,`is_enable`) values (1,'http://www.runoob.com/java/[\w|\d|-]+.html','#content>h1','#content',1,1);

/*
--测试sql 爬微信公众号的最新内容
insert  into `spider_site`(`site_id`,`type`,`name`,`url`,`domain`,`is_enable`) values (2,20,'巨国贤','https://mp.weixin.qq.com/s?','http://weixin.sogou.com/weixin?type=1&s_from=input&query=%E5%B7%A8%E5%9B%BD%E8%B4%A4',1);
insert  into `spider_rule`(`rule_id`,`regex`,`title_select`,`content_select`,`site_id`,`is_enable`) values (2,'https://mp.weixin.qq.com/profile?src=3&timestamp=1516936405&ver=1&signature=uPB8oKFEO2MOmtfGg3iI9tPNQr4rvXQravqPzRYhKmWoCu8ytfXavoF5Ty-s74CO5TmvFh8jf*w6jhIkLuBtXQ==','#page-content>#img-content>h2','#page-content>#img-content>.rich_media_content',2,1);
*/
