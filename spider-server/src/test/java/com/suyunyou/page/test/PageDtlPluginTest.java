package com.suyunyou.page.test;

import java.io.IOException;

import com.suyunyou.spider.model.Link;
import com.suyunyou.spider.model.PageDtl;
import com.suyunyou.spider.model.SpiderException;
import com.suyunyou.spider.plugins.IPagePlugin;
import com.suyunyou.spider.plugins.page.PageDtlPlugin;
import com.suyunyou.spider.utils.PageDownUtil;
import com.system.comm.utils.FrameJsonUtil;

/**
 * 接入用户信息
 * @author yuejing
 * @date 2017年12月5日 上午9:33:24
 */
public class PageDtlPluginTest {

	public static void main(String[] args) throws IOException, SpiderException {
		String link = "https://www.weixinqun.com/group?id=1076566";
		String regex = "https://www.weixinqun.com/group\\?id=[\\w|\\d|-]+";
		
		String titleSelect = ".clearfix>ul>li>.des_info_text>b";
		String contentSelect = ".cateNav>.cateNav_ny>.clearfix";
		IPagePlugin plugin = new PageDtlPlugin(regex, titleSelect, contentSelect);
		if (plugin.isHandle(link)) {
			System.out.println("验证通过，可以下载进入下载...");
			Link l = new Link();
			l.setLink(link);
			l.setContent(PageDownUtil.get(link));
			plugin.setLink(l);
			PageDtl page = plugin.setPageDtl();
			System.out.println(FrameJsonUtil.toString(page));
		} else {
			System.out.println("链接不符合规则，不能下载。");
		}
	}
	
}