package com.suyunyou.page.test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.suyunyou.SpiderConfig;
import com.system.auth.AuthUtil;
import com.system.comm.utils.FrameHttpUtil;

/**
 * 接入用户信息
 * @author yuejing
 * @date 2017年12月5日 上午9:33:24
 */
public class PageInfoPageQueryTest {

	public static void main(String[] args) throws IOException {
		String spiderTime = "2019-01-07";
		String siteId = "1";
		String result = pageQuery(1, 10, spiderTime, siteId);
		System.out.println(result);
	}
	
	/**
	 * 获取爬取的内容
	 * @param page
	 * @param size
	 * @param spiderTime
	 * @param siteId
	 * @return
	 * @throws IOException
	 */
	private static String pageQuery(Integer page, Integer size, String spiderTime, String siteId) throws IOException {
		String clientId = SpiderConfig.clientId;
		String time = String.valueOf(System.currentTimeMillis());
		String sercret = SpiderConfig.sercret;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("clientId", clientId);
		params.put("time", time);
		params.put("sign", AuthUtil.auth(clientId, time, sercret));

		params.put("page", page);
		params.put("size", size);
		params.put("spiderTime", spiderTime);
		params.put("siteId", siteId);
		return FrameHttpUtil.post(SpiderConfig.host + "/pageInfo/pageQuery", params);
	}
	
}