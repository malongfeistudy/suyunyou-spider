package com.suyunyou.manager.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.suyunyou.manager.dao.SpiderSiteDao;
import com.suyunyou.manager.pojo.SpiderSite;
import com.system.cache.redis.BaseCache;

@Component
public class SpiderSiteCache extends BaseCache {

	@Autowired
	private SpiderSiteDao spiderSiteDao;

	private String getKey(Integer siteId) {
		return "site_" + siteId;
	}
	public SpiderSite get(Integer siteId) {
		String key = getKey(siteId);
		SpiderSite site = super.get(key);
		if (site == null) {
			site = spiderSiteDao.get(siteId);
			super.set(key, site);
		}
		return site;
	}
	public void del(Integer siteId) {
		String key = getKey(siteId);
		super.delete(key);
	}


	
}
