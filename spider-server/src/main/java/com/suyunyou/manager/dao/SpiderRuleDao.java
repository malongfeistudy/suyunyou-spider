package com.suyunyou.manager.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.suyunyou.manager.pojo.SpiderRule;

public interface SpiderRuleDao {

	public abstract void save(SpiderRule spiderRule);

	public abstract void update(SpiderRule spiderRule);

	public abstract SpiderRule get(@Param("ruleId")Integer ruleId);

	public abstract List<SpiderRule> findBySiteId(@Param("siteId")Integer siteId);

	public abstract void delete(@Param("ruleId")Integer ruleId);

	public abstract List<SpiderRule> findEnable();

}