package com.suyunyou.manager.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.suyunyou.manager.pojo.SpiderLink;


public interface SpiderLinkDao {

	public abstract void save(SpiderLink spiderLink);

	public abstract SpiderLink getLink(@Param("link")String link);

	public abstract void updateContent(@Param("link")String link, @Param("content")String content, @Param("isFetcherContent") Integer isFetcherContent);

	public abstract void updateFetcherLink(@Param("link")String link, @Param("isFetcherLink")Integer isFetcherLink);

	public abstract void updateDownErrorNum(@Param("link")String link);
	
	public abstract void updateWaitFetcherContent(@Param("link")String link);

	public abstract Integer getDownErrorNum(@Param("link")String link);

	//public abstract String getWaitFetcherContent(@Param("startTime")Date startTime, @Param("downLinkErrorMaxNum")int downLinkErrorMaxNum);
	public abstract String getWaitFetcherContent(@Param("downLinkErrorMaxNum")int downLinkErrorMaxNum);

	public abstract void updateDbSuccIsFetcherContent(@Param("link")String link);

	public abstract void delete(@Param("linkId")Integer linkId);

	public abstract List<SpiderLink> findSpiderLink(SpiderLink spiderLink);

	public abstract int findSpiderLinkCount(SpiderLink spiderLink);

	public abstract void deleteBatch(@Param("siteId")Integer siteId, @Param("isFetcherContent")Integer isFetcherContent,
			@Param("isFetcherLink")Integer isFetcherLink);

	public abstract void updateDomainBySiteId(@Param("siteId")Integer siteId, @Param("domain")String domain);

}