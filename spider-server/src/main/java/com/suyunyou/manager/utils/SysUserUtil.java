package com.suyunyou.manager.utils;

import com.system.comm.utils.FrameMd5Util;

public class SysUserUtil {

	/**
	 * 加密密码
	 * @param password
	 * @return
	 */
	public static String encodePassword(String password) {
		String pwd = FrameMd5Util.getInstance().encodePassword(password);
		return pwd;
	}
}
