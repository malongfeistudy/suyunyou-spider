package com.suyunyou.manager.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.suyunyou.manager.pojo.PageInfo;
import com.suyunyou.manager.service.PageInfoService;
import com.suyunyou.manager.service.SpiderSiteService;
import com.system.comm.model.KvEntity;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 用户的Controller
 * @author  yuejing
 * @email   yuejing0129@163.com 
 * @net		www.suyunyou.com
 * @date    2015年3月30日 下午2:21:22 
 * @version 1.0.0
 */
@Controller
public class PageInfoUiController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PageInfoUiController.class);

	@Autowired
	private PageInfoService pageInfoService;
	@Autowired
	private SpiderSiteService spiderSiteService;
	
	@RequestMapping(value = "/pageInfo/f-view/manager")
	public String manger(HttpServletRequest request, ModelMap modelMap) {
		List<KvEntity> sites = spiderSiteService.findKvAll();
		modelMap.put("sites", sites);
		return "manager/page/info-manager";
	}

	@RequestMapping(value = "/pageInfo/f-view/look")
	public String look(HttpServletRequest request, ModelMap modelMap, Integer pageId) {
		PageInfo pageInfo = pageInfoService.getDtl(pageId);
		modelMap.put("pageInfo", pageInfo);
		return "manager/page/info-look";
	}

	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/pageInfo/f-json/pageQuery")
	@ResponseBody
	public ResponseFrame pageQuery(HttpServletRequest request, PageInfo pageInfo) {
		ResponseFrame frame = null;
		try {
			frame = pageInfoService.pageQuery(pageInfo);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL.getCode(), ResponseCode.FAIL.getMessage());
		}
		return frame;
	}

	/**
	 * 删除
	 * @return
	 */
	@RequestMapping(value = "/pageInfo/f-json/delete")
	@ResponseBody
	public ResponseFrame delete(HttpServletRequest request, Integer pageId) {
		ResponseFrame frame = new ResponseFrame();
		try {
			pageInfoService.delete(pageId);
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame.setCode(ResponseCode.FAIL.getCode());
			frame.setMessage(ResponseCode.FAIL.getMessage());
		}
		return frame;
	}
}
