package com.suyunyou.manager.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.suyunyou.comm.utils.RunSpiderUtil;
import com.suyunyou.manager.pojo.SpiderSite;
import com.suyunyou.manager.service.SpiderSiteService;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 提取网站的Controller
 * @author yuejing
 * @date 2016年6月26日 下午3:28:20
 * @version V1.0.0
 */
@Controller
public class SpiderSiteUiController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpiderSiteUiController.class);

	@Autowired
	private SpiderSiteService spiderSiteService;

	@RequestMapping(value = "/spiderSite/f-view/manager")
	public String manager(HttpServletRequest request, ModelMap modelMap) {
		return "manager/spider/site-manager";
	}
	
	/**
	 * 获取信息
	 * @return
	 */
	@RequestMapping(value = "/spiderSite/f-json/findAll")
	@ResponseBody
	public ResponseFrame findAll(HttpServletRequest request) {
		ResponseFrame frame = new ResponseFrame();
		List<SpiderSite> list = null;
		try {
			list = spiderSiteService.findAll();
			frame.setBody(list);
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("获取信息异常: " + e.getMessage(), e);
			frame.setCode(ResponseCode.FAIL.getCode());
			frame.setMessage(ResponseCode.FAIL.getMessage());
		}
		return frame;
	}

	/**
	 * 跳转到编辑页[包含新增和编辑]
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/spiderSite/f-view/edit")
	public String edit(HttpServletRequest request, ModelMap modelMap, Integer id) {
		if(id != null) {
			modelMap.put("spiderSite", spiderSiteService.get(id));
		}
		return "manager/spider/site-edit";
	}

	/**
	 * 保存
	 * @return
	 */
	@RequestMapping(value = "/spiderSite/f-json/save")
	@ResponseBody
	public ResponseFrame save(HttpServletRequest request, SpiderSite spiderSite) {
		ResponseFrame frame = new ResponseFrame();
		try {
			spiderSiteService.saveOrUpdate(spiderSite);
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("保存异常: " + e.getMessage(), e);
			frame.setCode(ResponseCode.FAIL.getCode());
			frame.setMessage(ResponseCode.FAIL.getMessage());
		}
		return frame;
	}

	/**
	 * 删除
	 * @return
	 */
	@RequestMapping(value = "/spiderSite/f-json/delete")
	@ResponseBody
	public ResponseFrame delete(HttpServletRequest request, Integer id) {
		ResponseFrame frame = new ResponseFrame();
		try {
			spiderSiteService.delete(id);
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame.setCode(ResponseCode.FAIL.getCode());
			frame.setMessage(ResponseCode.FAIL.getMessage());
		}
		return frame;
	}

	/**
	 * 删除
	 * @return
	 */
	@RequestMapping(value = "/spiderSite/f-json/resetSpider")
	@ResponseBody
	public ResponseFrame resetSpider(HttpServletRequest request, Integer id) {
		ResponseFrame frame = new ResponseFrame();
		try {
			RunSpiderUtil.reset();
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame.setCode(ResponseCode.FAIL.getCode());
			frame.setMessage(ResponseCode.FAIL.getMessage());
		}
		return frame;
	}
}