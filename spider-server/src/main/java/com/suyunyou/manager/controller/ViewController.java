package com.suyunyou.manager.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * view
 * @author yuejing
 * @date 2016-05-22 11:17:54
 * @version V1.0.0
 */
@Controller
public class ViewController extends BaseController {

	/**
	 * 首页
	 */
	@RequestMapping(value = {"/", "/index"})
	public String index(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
		return "redirect:index.jsp";
	}
}