package com.suyunyou.manager.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.suyunyou.manager.pojo.SpiderLink;
import com.suyunyou.manager.service.SpiderLinkService;
import com.suyunyou.manager.service.SpiderSiteService;
import com.system.comm.model.KvEntity;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 链接的Controller
 * @author  yuejing
 * @email   yuejing0129@163.com 
 * @net		www.suyunyou.com
 * @date    2015年3月30日 下午2:21:22 
 * @version 1.0.0
 */
@Controller
public class SpiderLinkUiController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpiderLinkUiController.class);

	@Autowired
	private SpiderLinkService spiderLinkService;
	@Autowired
	private SpiderSiteService spiderSiteService;
	
	@RequestMapping(value = "/spiderLink/f-view/manager")
	public String manger(HttpServletRequest request, ModelMap modelMap) {
		List<KvEntity> sites = spiderSiteService.findKvAll();
		modelMap.put("sites", sites);
		return "manager/spider/link-manager";
	}

	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/spiderLink/f-json/pageQuery")
	@ResponseBody
	public ResponseFrame pageQuery(HttpServletRequest request, SpiderLink spiderLink) {
		ResponseFrame frame = null;
		try {
			frame = spiderLinkService.pageQuery(spiderLink);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL.getCode(), ResponseCode.FAIL.getMessage());
		}
		return frame;
	}

	/**
	 * 删除
	 * @return
	 */
	@RequestMapping(value = "/spiderLink/f-json/delete")
	@ResponseBody
	public ResponseFrame delete(HttpServletRequest request, Integer linkId) {
		ResponseFrame frame = new ResponseFrame();
		try {
			spiderLinkService.delete(linkId);
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame.setCode(ResponseCode.FAIL.getCode());
			frame.setMessage(ResponseCode.FAIL.getMessage());
		}
		return frame;
	}

	/**
	 * 批量删除
	 * @return
	 */
	@RequestMapping(value = "/spiderLink/f-json/deleteBatch")
	@ResponseBody
	public ResponseFrame deleteBatch(HttpServletRequest request, Integer siteId,
			Integer isFetcherContent, Integer isFetcherLink) {
		ResponseFrame frame = new ResponseFrame();
		try {
			spiderLinkService.deleteBatch(siteId, isFetcherContent, isFetcherLink);
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame.setCode(ResponseCode.FAIL.getCode());
			frame.setMessage(ResponseCode.FAIL.getMessage());
		}
		return frame;
	}
}