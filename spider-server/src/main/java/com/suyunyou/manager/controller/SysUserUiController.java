package com.suyunyou.manager.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.suyunyou.manager.pojo.SysUser;
import com.suyunyou.manager.service.SysUserService;
import com.system.comm.model.Page;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 用户的Controller
 * @author  yuejing
 * @email   yuejing0129@163.com 
 * @net		www.suyunyou.com
 * @date    2015年3月30日 下午2:21:22 
 * @version 1.0.0
 */
@Controller
public class SysUserUiController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SysUserUiController.class);

	@Autowired
	private SysUserService sysUserService;
	
	/**
	 * ajax登录
	 * @return
	 */
	@RequestMapping(value = "/sysUser/json/login")
	@ResponseBody
	public void login(HttpServletRequest request, HttpServletResponse response, SysUser sysUser) {
		ResponseFrame frame = null;
		try {
			frame = sysUserService.login(sysUser);
			if(ResponseCode.SUCC.getCode() == frame.getCode().intValue()) {
				setSessionSysUser(request, (SysUser) frame.getBody());
			}
			frame.setBody(null);
		} catch (Exception e) {
			LOGGER.error("登录异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	
	/**
	 * 退出
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/sysUser/f-view/exit")
	public String exit(HttpServletRequest request) {
		removeSessionSysUser(request);
		return "redirect:/index.jsp";
	}
	
	/**
	 * 跳转到管理页
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/sysUser/f-view/main")
	public String main(HttpServletRequest request) {
		return "manager/main";
	}
	
	@RequestMapping(value = "/sysUser/f-view/manager")
	public String manger(HttpServletRequest request) {
		return "manager/user/user-manager";
	}

	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/sysUser/f-json/pageQuery")
	@ResponseBody
	public ResponseFrame pageQuery(HttpServletRequest request, SysUser sysUser) {
		ResponseFrame frame = new ResponseFrame();
		Page<SysUser> page = null;
		try {
			page = sysUserService.pageQuery(sysUser);
			frame.setBody(page);
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
			frame.setCode(ResponseCode.FAIL.getCode());
			frame.setMessage(ResponseCode.FAIL.getMessage());
		}
		return frame;
	}

	/**
	 * 跳转到编辑页[包含新增和编辑]
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/sysUser/f-view/edit")
	public String edit(HttpServletRequest request, ModelMap modelMap, Integer id) {
		if(id != null) {
			modelMap.put("sysUser", sysUserService.get(id));
		}
		return "manager/user/user-edit";
	}

	/**
	 * 保存
	 * @return
	 */
	@RequestMapping(value = "/sysUser/f-json/save")
	@ResponseBody
	public ResponseFrame save(HttpServletRequest request, SysUser sysUser) {
		ResponseFrame frame = new ResponseFrame();
		SysUser user = getSessionSysUser(request);
		try {
			if(sysUser.getId() == null) {
				sysUser.setAdduser(user.getId());
				sysUserService.save(sysUser);
			} else {
				sysUserService.update(sysUser);
			}
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("保存异常: " + e.getMessage(), e);
			frame.setCode(ResponseCode.FAIL.getCode());
			frame.setMessage(ResponseCode.FAIL.getMessage());
		}
		return frame;
	}
	
	/**
	 * 删除
	 * @return
	 */
	@RequestMapping(value = "/sysUser/f-json/delete")
	@ResponseBody
	public ResponseFrame delete(HttpServletRequest request, Integer id) {
		ResponseFrame frame = new ResponseFrame();
		try {
			sysUserService.delete(id);
			frame.setSucc();
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame.setCode(ResponseCode.FAIL.getCode());
			frame.setMessage(ResponseCode.FAIL.getMessage());
		}
		return frame;
	}
}
