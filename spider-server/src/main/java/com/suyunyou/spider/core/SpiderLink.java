package com.suyunyou.spider.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.suyunyou.manager.enums.Config;
import com.suyunyou.manager.service.SysConfigService;
import com.suyunyou.spider.task.SpiderLinkTask;
import com.system.comm.utils.FrameSpringBeanUtil;
import com.system.threadpool.FrameThreadPool;
import com.system.threadpool.FrameThreadPoolUtil;

/**
 * 爬取地址
 * @author 岳静
 * @date 2016年6月24日 下午2:05:18 
 * @version V1.0
 */
public class SpiderLink {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpiderLink.class);
	private boolean run = true;
	private FrameThreadPool threadPool;
	private SysConfigService configService;
	public SpiderLink() {
		threadPool = FrameThreadPoolUtil.getThreadPool("spider-link");
		configService = FrameSpringBeanUtil.getBean(SysConfigService.class);
	}

	/**
	 * 开启爬取任务
	 */
	public void start() {
		LOGGER.info("进入下载链接页面的任务");
		final int sleep = Integer.valueOf(configService.getValue(Config.SLEEP_SPIDER_LINK, "500"));
		final int downLinkErrorMaxNum = Integer.valueOf(configService.getValue(Config.DOWN_LINK_ERROR_MAX_NUM, "2"));
		final int linkRepeatExtractHour = Integer.valueOf(configService.getValue(Config.LINK_REPEAT_EXTRACT_HOUR, "6"));
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (run) {
					threadPool.execute(new SpiderLinkTask(downLinkErrorMaxNum, linkRepeatExtractHour));
					try {
						Thread.sleep(sleep);
					} catch (InterruptedException e) {
						LOGGER.error("休眠异常");
					}
				}
			}
		}).start();
	}

	/**
	 * 停止
	 */
	public void stop() {
		run = false;
		threadPool.destroy();
	}
}