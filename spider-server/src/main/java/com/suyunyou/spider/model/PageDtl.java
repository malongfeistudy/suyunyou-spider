package com.suyunyou.spider.model;

import java.util.Date;

/**
 * 页面对象
 * @author yuejing
 * @date 2016年6月25日 下午4:41:52
 * @version V1.0.0
 */
public class PageDtl {

	private String title;
	private String content;
	private String author;
	private Date createTime;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}