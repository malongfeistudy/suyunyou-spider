package com.suyunyou.spider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.suyunyou.spider.core.AnalysisLink;
import com.suyunyou.spider.core.FetcherPage;
import com.suyunyou.spider.core.SpiderLink;
import com.suyunyou.spider.utils.PluginUtil;

public class Spider {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Spider.class);
	private SpiderLink link;
	private AnalysisLink analysis;
	private FetcherPage fetcher;

	/**
	 * 运行
	 */
	public void run() {
		//初始链接提取插件
		PluginUtil.initLinkPlugin();
		
		//爬取地址
		link = new SpiderLink();
		link.start();
		
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			LOGGER.error("休眠异常");
		}
		//提取子链接
		analysis = new AnalysisLink();
		analysis.start();
		
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			LOGGER.error("休眠异常");
		}
		//提取需要的内容
		fetcher = new FetcherPage();
		fetcher.start();
	}
	
	/**
	 * 停止
	 */
	public void stop() {
		if (link != null) {
			link.stop();
			link = null;
		}
		if (analysis != null) {
			analysis.stop();
			analysis = null;
		}
		if (fetcher != null) {
			fetcher.stop();
			fetcher = null;
		}
	}
}