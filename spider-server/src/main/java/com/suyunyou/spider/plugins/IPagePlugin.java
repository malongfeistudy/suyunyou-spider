package com.suyunyou.spider.plugins;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.suyunyou.manager.service.PageInfoService;
import com.suyunyou.spider.model.Link;
import com.suyunyou.spider.model.PageDtl;
import com.suyunyou.spider.model.SpiderException;
import com.system.comm.utils.FrameSpringBeanUtil;

/**
 * 详情页面的插件接口
 * @author yuejing
 * @date 2016年6月25日 下午4:38:39
 * @version V1.0.0
 */
public abstract class IPagePlugin {

	private Link link;
	//提取目标的url规则
	private String targetRegex;
	
	/**
	 * 初始化信息
	 * @param link			提取的链接
	 */
	public void setLink(Link link) {
		this.link = link;
	}
	/**
	 * 初始化信息
	 * @param targetRegex	设置提取目标的url的规则
	 */
	public void setTargetRegex(String targetRegex) {
		this.targetRegex = targetRegex;
	}
	
	public Link getLink() {
		return link;
	}
	/**
	 * 插件处理文章信息
	 * @return
	 */
	public abstract PageDtl setPageDtl() throws SpiderException;
	
	/**
	 * 是否需要处理信息
	 * @return
	 */
	public boolean isHandle(String link) {
		Pattern p = Pattern.compile(targetRegex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = p.matcher(link);
		while (matcher.find()) {
			return true;
		}
		return false;
	}
	
	/**
	 * 处理获取文章详情
	 * @return
	 * @throws SpiderException 
	 */
	public boolean handlePageDtl() throws SpiderException {
		//匹配是否符合提取
		if(!isHandle(link.getLink())) {
			return false;
		}
		PageDtl page = setPageDtl();
		if(page != null) {
			//保存页面信息
			PageInfoService pageInfoService = FrameSpringBeanUtil.getBean(PageInfoService.class);
			pageInfoService.save(link.getSiteId(), page.getTitle(), page.getContent(), link.getContent(), page.getAuthor(), link.getLink());
		}
		return true;
	}
}