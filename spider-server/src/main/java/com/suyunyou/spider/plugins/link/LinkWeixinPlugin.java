package com.suyunyou.spider.plugins.link;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.suyunyou.spider.plugins.ILinkPlugin;
import com.system.comm.utils.FrameJsonUtil;

/**
 * 默认提取
 * @author yuejing
 * @date 2018年1月26日 下午2:42:20
 */
public class LinkWeixinPlugin extends ILinkPlugin {

	@Override
	public List<String> getLinks(String content) {
		//"content_url":"/s?timestamp=1516943628&amp;",
		List<String> list = new ArrayList<String>();
		Pattern pattern = Pattern.compile("(?i)(?s)\"content_url\":\"(.*?)\"");
		Matcher matcher = pattern.matcher(content);

		while (matcher.find()) {
			list.add(matcher.group(1).replace("&amp;", "&"));
		}
		return list;
	}
	
	public static void main(String[] args) {
		String content = ",\"digest\":\"吃货一天天的，操碎了心！\",\"fileid\":503811441,\"is_multi\":1,\"multi_app_msg_item_list\":[],\"source_url\":\"http://dwz.cn/4r75pZ\",\"subtype\":9,\"title\":\"瑞士：活煮龙虾违法！中国吃货：偷摸吃，别让龙虾报警！\"},\"comm_msg_info\":{\"content\":\"\",\"datetime\":1516183206,\"fakeid\":\"3297052336\",\"id\":1000000589,\"status\":2,\"type\":49}},{\"app_msg_ext_info\":{\"author\":\"\",\"content\":\"\",\"content_url\":\"/s?timestamp=1516943628&amp;src=3&amp;ver=1&amp;signature=GX2eJl2HHGyxIOdObD0FbA8Etx96ejCooFSz*Ie2YJ*bkK0wCKIbw7Akb*kkkfAdJsx6-9WaZ4Re3*y78CM0-5Y4GMpGOXtTGF-zvTk54JVLjK62YSi3jt86kkdeA74iBYYeo6JpqYp5aV4pCLnLTYob8WXEaVHFpGfZBC5-p94=\",\"copyright_stat\":100,\"cover\":\"http://mmbiz.qpic.cn/mmbiz_jpg/wUWibib5ee7Iw02AK7rPOWelkjnQq4fN8NibbjLorZTJ5dX4FMGVLO8N2hEplDe6qWG0vkEADMSXDEoSbjPxGyOFg/0?wx_fmt=jpeg\",\"del_flag\":1,\"digest\":\"\",\"fileid\":503811435,\"is_multi\":1,\"multi_app_msg_item_list\":[],\"source_url\":\"https://mp.weixin.qq.com/mp/profile_ext?action=home&amp;__biz=MzI5NzA1MjMzNg==#wechat_redirect\",\"subtype\":9,\"title\":\"妈妈为我安排了一次此生难忘的……哈哈哈是亲生的没错了！！\"},\"comm_msg_info\":{\"content\":\"\",\"datetime\":1516096800,\"fakeid\":\"3297052336\",\"id\":1000000588,\"status\":2,\"type\":49}}]};";
		List<String> links = new LinkWeixinPlugin().getLinks(content);
		System.out.println(FrameJsonUtil.toString(links));
	}

}