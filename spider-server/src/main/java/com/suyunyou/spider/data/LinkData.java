package com.suyunyou.spider.data;

import java.util.Date;

import com.suyunyou.manager.pojo.SpiderLink;
import com.suyunyou.manager.pojo.SpiderSite;
import com.suyunyou.manager.service.SpiderLinkService;
import com.suyunyou.spider.model.Link;
import com.system.comm.enums.Boolean;
import com.system.comm.utils.FrameMd5Util;
import com.system.comm.utils.FrameSpringBeanUtil;
import com.system.comm.utils.FrameTimeUtil;

/**
 * 地址相关的数据
 * @author 岳静
 * @date 2016年6月24日 下午3:06:39 
 * @version V1.0
 */
public class LinkData {

	private static SpiderLinkService getDb() {
		return FrameSpringBeanUtil.getBean(SpiderLinkService.class);
	}
	private static Link getDbLink(String link) {
		SpiderLink sl = getDb().getLink(link);
		if(sl == null) {
			return null;
		}
		Link l = new Link();
		l.setLink(sl.getLink());
		l.setDomain(sl.getDomain());
		l.setCreateTime(sl.getCreateTime());
		l.setContent(sl.getContent());
		l.setFetcherContentTime(sl.getFetcherContentTime());
		l.setFetcherLinkTime(sl.getFetcherLinkTime());
		l.setIsFetcherLink(sl.getIsFetcherLink());
		l.setSiteId(sl.getSiteId());
		l.setDownErrorNum(sl.getDownErrorNum());
		l.setSiteUrl(sl.getSite().getUrl());
		return l;
	}
	private static void saveDbLink(Link l) {
		getDb().save(l.getLink(), l.getDomain(), l.getSiteId());
	}
	private static void updateDbContent(String link, String content) {
		getDb().updateContent(link, content);
	}
	private static void updateDbFetcherLink(String link, Integer isFetcherLink) {
		getDb().updateFetcherLink(link, isFetcherLink);
	}
	private static void updateWaitFetcherContent(String link) {
		getDb().updateWaitFetcherContent(link);
	}
	private static void updateDbSuccIsFetcherContent(String link) {
		getDb().updateDbSuccIsFetcherContent(link);
	}
	
	/**
	 * 添加域名
	 * @param link
	 * @param siteId
	 */
	public static void addSite(String link, Integer siteId) {
		Link l = get(link);
		if(l == null) {
			l = new Link();
			l.setLink(link);
			l.setDomain(SiteData.get(siteId).getDomain());
			l.setFetcherContentTime(FrameTimeUtil.getTime());
			l.setCreateTime(FrameTimeUtil.getTime());
			l.setIsFetcherLink(Boolean.FALSE.getCode());
			l.setSiteId(siteId);
			saveDbLink(l);
		} else {
			updateWaitFetcherContent(link);
		}
	}
	/**
	 * 添加提取的地址
	 * @param link
	 * @param siteId
	 */
	public static void addFetcherLink(String link, Integer siteId) {
		SpiderSite site = SiteData.get(siteId);
		if (site == null) {
			return;
		}
		Link l = get(link);
		if(l == null) {
			l = new Link();
			l.setLink(link);
			l.setDomain(site.getDomain());
			l.setCreateTime(FrameTimeUtil.getTime());
			l.setIsFetcherLink(Boolean.FALSE.getCode());
			l.setSiteId(siteId);
			saveDbLink(l);
		}
		//提取的信息不存在、并且待提取的也不存在才添加
		//FetcherLinkData.add(link);
	}

	/**
	 * 添加提取后的链接内容
	 * @param link
	 * @param content
	 * @param isUpdateDb	是否需要更新数据库内容
	 */
	public static void addLinkFetcherContent(String link, String content, boolean isUpdateDb) {
		//移除待提取的链接
		//FetcherLinkData.remove(link);

		//跟新提取后的内容
		if(isUpdateDb) {
			updateDbContent(link, content);
		} else {
			updateDbSuccIsFetcherContent(link);
		}

		//添加待提取子链接的任务
		FetcherAnalysisLinkData.add(link);

		//添加待分析地址获取页面内容，执行FetcherPageTask内调用的数据源
		FetcherPageLinkData.add(link);
	}

	/**
	 * 获取对象
	 * @param link
	 * @return
	 */
	public static Link get(String link) {
		return getDbLink(link);
	}

	/**
	 * 更新分析子链接信息
	 * @param link
	 * @param isFetcherLink
	 * @param fetcherLinkTime
	 */
	public static void updateFetcherAnalysisLink(String link, Integer isFetcherLink, Date fetcherLinkTime) {
		/*String key = keyLinks(link);
		Link l = getCache().get(key);
		l.setIsFetcherLink(isFetcherLink);
		l.setFetcherLinkTime(fetcherLinkTime);
		getCache().set(key, l);*/
		updateDbFetcherLink(link, isFetcherLink);
	}
	/*//key的前缀
	private final static String PREFIX = "LINKS";

	private static BaseCache getCache() {
		return BaseCache.getInstance();
	}
	private static String keyLinks(String link) {
		return getCache().getKey(PREFIX, FrameMd5Util.getInstance().encodePassword(link));
	}
	 *//**
	 * 添加提取的地址
	 * @param link
	 * @param siteId
	 *//*
	public static void addFetcherLink(String link, Integer siteId) {
		String key = keyLinks(link);
		Link l = getCache().get(key);
		if(l == null) {
			l = new Link();
			l.setLink(link);
			l.setDomain(getDomain(link));
			l.setCreateTime(FrameTimeUtil.getTime());
			l.setIsFetcherLink(Boolean.FALSE.getCode());
			l.setSiteId(siteId);
			getCache().set(key, l);
		}
		//提取的信息不存在、并且待提取的也不存在才添加
		FetcherLinkData.add(link);
	}

	  *//**
	  * 获取域名（如：http://www.suyunyou.com）
	  * @param link
	  * @return
	  *//*
	private static String getDomain(String link) {
		Pattern p = Pattern.compile(DOMAIN_REG, Pattern.CASE_INSENSITIVE);
		Matcher matcher = p.matcher(link);
		while (matcher.find()) {
			return DOMAIN_HTTP + matcher.group(0);
		}
		return link;
	}

	   *//**
	   * 添加提取后的链接内容
	   * @param link
	   * @param content
	   *//*
	public static void addLinkFetcherContent(String link, String content) {
		//移除待提取的链接
		FetcherLinkData.remove(link);

		//跟新提取后的内容
		String key = keyLinks(link);
		Link l = getCache().get(key);
		l.setContent(content);
		l.setFetcherContentTime(FrameTimeUtil.getTime());
		getCache().set(key, l);

		//添加待提取的任务
		FetcherAnalysisLinkData.add(link);

		//添加待分析的地址
		FetcherPageLinkData.add(link);
	}

	    *//**
	    * 获取对象
	    * @param link
	    * @return
	    *//*
	public static Link get(String link) {
		String key = keyLinks(link);
		return getCache().get(key);
	}

	     *//**
	     * 更新分析子链接信息
	     * @param link
	     * @param isFetcherLink
	     * @param fetcherLinkTime
	     *//*
	public static void updateFetcherAnalysisLink(String link, Integer isFetcherLink, Date fetcherLinkTime) {
		String key = keyLinks(link);
		Link l = getCache().get(key);
		l.setIsFetcherLink(isFetcherLink);
		l.setFetcherLinkTime(fetcherLinkTime);
		getCache().set(key, l);
	}*/

	/**
	 * 链接的详细信息
	 */
	//private static Map<String, Link> links = new HashMap<String, Link>();

	/**
	 * 添加提取的地址
	 * @param link
	 * @param siteId
	 *//*
	public static synchronized void addFetcherLink(String link, Integer siteId) {
		Link l = links.get(link);
		if(l == null) {
			//提取的信息不存在、并且待提取的也不存在才添加
			FetcherLinkData.add(link);
			l = new Link();
			l.setLink(link);
			l.setDomain(getDomain(link));
			l.setCreateTime(FrameTimeUtil.getTime());
			l.setIsFetcherLink(Boolean.FALSE.getCode());
			l.setSiteId(siteId);
			links.put(link, l);
		}
	}

	  *//**
	  * 获取域名（如：http://www.suyunyou.com）
	  * @param link
	  * @return
	  *//*
	private static String getDomain(String link) {
		Pattern p = Pattern.compile(DOMAIN_REG, Pattern.CASE_INSENSITIVE);
		Matcher matcher = p.matcher(link);
		while (matcher.find()) {
			return DOMAIN_HTTP + matcher.group(0);
		}
		return link;
	}

	   *//**
	   * 添加提取后的链接内容
	   * @param link
	   * @param content
	   *//*
	public static synchronized void addLinkFetcherContent(String link, String content) {
		//移除待提取的链接
		FetcherLinkData.remove(link);

		//跟新提取后的内容
		Link l = links.get(link);
		l.setContent(content);
		l.setFetcherContentTime(FrameTimeUtil.getTime());
		links.put(link, l);

		//添加待提取的任务
		FetcherAnalysisLinkData.add(link);

		//添加待分析的地址
		FetcherPageLinkData.add(link);
	}

	    *//**
	    * 获取对象
	    * @param link
	    * @return
	    *//*
	public static Link getLink(String link) {
		return links.get(link);
	}

	     *//**
	     * 更新分析子链接信息
	     * @param link
	     * @param isFetcherLink
	     * @param fetcherLinkTime
	     *//*
	public static synchronized void updateFetcherAnalysisLink(String link, Integer isFetcherLink, Date fetcherLinkTime) {
		Link l = getLink(link);
		l.setIsFetcherLink(isFetcherLink);
		l.setFetcherLinkTime(fetcherLinkTime);
		links.put(link, l);
	}*/

	public static void main(String[] args) {
		//String link = "http://www.linuxidc.com/entry/4545/0/";
		//System.out.println(getDomain(link));

		System.out.println(FrameMd5Util.getInstance().encodePassword("http://www.suyunyou.com/s11.html"));
	}
}