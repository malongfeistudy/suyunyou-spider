JUtil.sys = {
		//跳转到指定地址
		location : function(url, islogin, isback) {
			if(islogin && user.uname==='') { JUtil.open.login(); return; }
			if(isback) {
				if(url.indexOf('?')!=-1) url += '&';
				else url += '?';
				url += 'back='+location;
			}
			location = url;
		},
		//初始化系统资料
		init : function() {
			if(config.initHmId)
				JUtil.sys.initHeader(config.initHmId);
		},
		//初始化header的选中
		initHeader : function(id) {
			$('#headerMenu>ul>li').each(function(i, obj) {
				if($(obj).attr('id') == id) {
					$(obj).addClass('active');
					return true;
				}
			});
		},
		resetSpider : function() {
			if(confirm('重启后，会根据新的规则爬取，您确定要重启网络爬虫吗?')) {
				JUtil.ajax({
					url : webroot + '/spiderSite/f-json/resetSpider',
					data : { },
					success : function(json) {
						if (json.code === 0) {
							message('重启网络爬虫成功');
						}
						else if (json.code === -1)
							message(JUtil.msg.ajaxErr);
						else
							message(json.message);
					}
				});
			}
		}
};