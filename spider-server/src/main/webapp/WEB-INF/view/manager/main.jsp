<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="/WEB-INF/tld/c.tld"%>
<%@ taglib prefix="my" uri="/WEB-INF/tld/my.tld"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-${user.nickname}</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/WEB-INF/view/inc/header.jsp"></jsp:include>
	<div class="container">
		<jsp:include page="/WEB-INF/view/manager/comm/left.jsp"></jsp:include>
		<div class="c-right">
			<div class="panel panel-success">
				<div class="panel-heading">欢迎您 <b><a href="${webroot}/sysUser/f_view/main.shtml">${user.nickname}</a></b></div>
				<div class="panel-body">
					<div class="row" id="infoPanel"></div>
					<div id="infoPage" class="table-page-panel">欢迎进入速云优网络爬虫管理系统!</div>
				</div>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
</body>
</html>