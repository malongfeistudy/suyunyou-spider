<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="my" uri="/WEB-INF/tld/my.tld" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-提取网站管理</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/WEB-INF/view/inc/header.jsp"></jsp:include>
	<div class="container">
		<jsp:include page="/WEB-INF/view/manager/comm/left.jsp">
			<jsp:param name="first" value="spider"/>
			<jsp:param name="second" value="siteMgr"/>
		</jsp:include>
		<div class="c-right">
			<div class="panel panel-success">
				<div class="panel-heading panel-heading-tool">
					<div class="row">
						<div class="col-sm-5 title">提取任务 / <b>提取网站</b></div>
						<div class="col-sm-7 text-right">
							<div class="btn-group">
						  		<a href="javascript:location.reload()" class="btn btn-default btn-sm">刷新</a>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body">
					<div class="table-tool-panel">
						<div class="row">
							<div class="col-sm-6 enter-panel">
								<%-- <div class="btn-group">
									<my:select id="siteId" items="${sites}" cssCls="form-control input-sm" value="" exp="style=\"width:220px;\""/>
								</div>
								<input type="text" style="width: 150px;display: inline;" class="form-control input-sm" id="receUserId" placeholder="接收人编码[精确查找]" value="">
							  	<button type="button" class="btn btn-sm btn-default enter-fn" onclick="info.loadInfo(1)">查询</button> --%>
							</div>
							<div class="col-sm-6 text-right">
							  	<div class="btn-group">
								  	<a href="javascript:;" class="btn btn-success btn-sm" onclick="info.edit()">新增</a>
							  	</div>
							  	<div class="btn-group">
								  	<a href="javascript:;" class="btn btn-warning btn-sm" onclick="JUtil.sys.resetSpider()">重启网络爬虫</a>
							  	</div>
							</div>
						</div>
				  	</div>
					<div id="infoPanel" class="table-panel"></div>
					<div id="infoPage" class="table-page-panel"></div>
				</div>
			</div>
		</div>
		<br clear="all">
	</div>
	<jsp:include page="/WEB-INF/view/inc/footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
<script type="text/javascript">
var info = {
		//获取用户信息
		loadInfo : function() {
			JUtil.ajax({
				url : '${webroot}/spiderSite/f-json/findAll',
				data : { },
				success : function(json){
					var _panel = $('#infoPanel').empty();
					if(json.code === 0) {
						var _content = ['<table class="table table-striped table-hover"><thead><tr class="info">',
				                         '<th>编号</th>',
				                         '<th>类型</th>',
				                         '<th title="用于匹配前面链接一致的参数">名称</th>',
				                         '<th>域名</th>',
				                         '<th width="150">操作</th>',
				                         '</tr></thead><tbody>'];
						
						$.each(json.body, function(i, obj) {
							_content.push('<tr>',
							    	'<td>',obj.siteId,'</td>',
							    	'<td>',obj.typeName,'</td>',
							    	'<td title="',obj.url,'">', obj.isEnable === 1 ? '<label class="label label-success">激活</label>':'<label class="label label-default">冻结</label>','&nbsp;',obj.name,'</td>',
									'<td>',obj.domain,'</td>',
							    	'<td><a class="glyphicon glyphicon-edit text-success" href="javascript:info.edit(',obj.siteId,')" title="修改"></a> ',
							    	'&nbsp; <a class="glyphicon glyphicon-remove text-success" href="javascript:info.del(',obj.siteId,')" title="删除"></a>',
							    	'&nbsp; &nbsp; <a class="glyphicon text-success" href="',webroot,'/spiderRule/f-view/manager?siteId=',obj.siteId,'">规则管理</a> ',
							    	'</td>',
								'</tr>');
						});

						_content.push('</tbody></table>');
						_panel.append(_content.join(''));
					}
					else _panel.append('没有记录噢');
				}
			});
		},
		//编辑
		edit : function(id) {
			dialog({
				title: '编辑',
				url: webroot + '/spiderSite/f-view/edit?id='+(id?id:''),
				type: 'iframe',
				width: 420,
				height: 440
			});
		},
		del : function(id) {
			if(confirm('网站被删除后，其对应的提取规则都会被删除，您确定要删除吗?')) {
				JUtil.ajax({
					url : '${webroot}/spiderSite/f-json/delete',
					data : { id: id },
					success : function(json) {
						if (json.code === 0) {
							message('删除成功');
							info.loadInfo();
						}
						else if (json.code === -1)
							message(JUtil.msg.ajaxErr);
						else
							message(json.message);
					}
				});
			}
		}
};
$(function() {
	info.loadInfo();
});
</script>
</body>
</html>