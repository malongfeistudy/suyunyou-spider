<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/inc/sys.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-编辑网站</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body class="cld-body">
	<div class="enter-panel ep-xs">
		<input type="hidden" id="siteId" value="${spiderSite.siteId}">
		<div class="form-group">
			<my:select id="type" dictcode="spider_site_type" cssCls="form-control" value="${spiderSite.type}"/>
		</div>
  		<div class="form-group">
			<input type="text" class="form-control" id="name" placeholder="名称" value="${spiderSite.name}">
		</div>
  		<div class="form-group">
			<input type="text" class="form-control" id="url" placeholder="爬取入口" value="${spiderSite.url}">
		</div>
  		<div class="form-group">
			<input type="text" class="form-control" id="domain" placeholder="域名(用于匹配前面链接一致的参数)" value="${spiderSite.domain}">
		</div>
  		<div class="form-group">执行状态：&nbsp; &nbsp; <my:radio id="isEnable" name="isEnable" dictcode="boolean" value="${spiderSite.isEnable}" defvalue="1" /></div>
  		<div class="form-group">
			<input type="text" class="form-control" id="rule" placeholder="爬取调度规则" value="${spiderSite.rule}">
		</div>
		<div class="text-right"><small>
			<a href="javascript:;" onclick="$('#rule').val('fixed-hour:09:30,12:40')" title="9点30分和12点40分重新爬入口页">每天9:30,12:40</a> |
			<a href="javascript:;" onclick="$('#rule').val('interval-hour:1')" title="每隔1小时重新爬入口页">每隔1小时</a> |
			<a href="javascript:;" onclick="$('#rule').val('interval-min:30')" title="每隔30分钟重新爬入口页">每隔30分钟</a></small>
		</div>
  		<div class="footer-operate">
			<span id="saveMsg" class="label label-danger"></span>
 			<div class="btn-group">
				<button type="button" id="saveBtn" class="btn btn-success enter-fn">保存</button>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<script type="text/javascript">
	$(function() {
		$('#saveBtn').click(function() {
			var _saveMsg = $('#saveMsg').empty();
			var _name = $('#name');
			if(JUtil.isEmpty(_name.val())) {
				_saveMsg.append('请输入名称');
				_name.focus();
				return;
			}
			var _url = $('#url');
			if(JUtil.isEmpty(_url.val())) {
				_saveMsg.append('请输入爬取的地址');
				_url.focus();
				return;
			}
			var _domain = $('#domain');
			if(JUtil.isEmpty(_domain.val())) {
				_saveMsg.append('请输入域名');
				_domain.focus();
				return;
			}

			var _saveBtn = $('#saveBtn');
			var _orgVal = _saveBtn.html();
			_saveBtn.attr('disabled', 'disabled').html('保存中...');
			var _isEnable = $('input[name="isEnable"]:checked').val();
			JUtil.ajax({
				url : '${webroot}/spiderSite/f-json/save',
				data : {
					siteId: $('#siteId').val(),
					type: $('#type').val(),
					name: _name.val(),
					rule: $('#rule').val(),
					domain: _domain.val(),
					url: _url.val(),
					isEnable: _isEnable
				},
				success : function(json) {
					if (json.code === 0) {
						_saveMsg.attr('class', 'label label-success').append('保存成功');
						setTimeout(function() {
							parent.info.loadInfo();
							parent.dialog.close();
						}, 800);
					}
					else if (json.code === -1)
						_saveMsg.append(JUtil.msg.ajaxErr);
					else
						_saveMsg.append(json.message);
					_saveBtn.removeAttr('disabled').html(_orgVal);
				}
			});
		});
	});
	</script>
</body>
</html>