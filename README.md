﻿# 速云优网络爬虫 - 功能简介
```
速云优网站的垂直爬取网页程序，提供UI来管理爬取的网站和爬取文章内容的规则。
通过jsoup来获取标题和内容，主要是通过选择器获取。
链接的提取通过正则表达式来匹配，[如：http://www.runoob.com/java/[\w|\d|-]+.html] 代表匹配java/后面的任意字符的html结尾的地址。
标题选择器：#content>h1，代表获取内容里ID为content，并找到下面的h1标签的内容为标题
内容选择器：#content，代表获取内容里ID为content的内容为内容
爬取的内容会保存到数据库中[具体表为page_info]。
如果需要更多的提取页面内容规则可以在[com.suyunyou.spider.plugins.page]包下写自己的插件，在SpiderUtil.addSiteFetcherPage(...)方法中添加使用。
如果需要更多的提取内容的链接的规则可以在[com.suyunyou.spider.plugins.link]包下写自己的插件。

提取微信公众号信息，通过LinkWeixinPlugin插件提炼链接
```

### 初始程序
初始程序，先执行创建数据库名称spider、用户名root密码root，表结构会在程序启动时自动创建。
数据库链接在[application.properties]文件中修改。
启动redis，修改resources下的application.properties文件中的redis配置的信息。
然后执行测试爬虫的初始数据的脚本[doc/init.sql]。

```
登录地址：http://127.0.0.1:6080/
账户：admin
密码：123456
```

爬取网站的维护页面
![爬取网站管理](https://gitee.com/uploads/images/2018/0126/104405_1a755e57_126057.jpeg "爬取网站管理")

网站爬取页面规则管理的页面
![网站爬取页面规则管理](https://gitee.com/uploads/images/2018/0126/104511_ac49b5ce_126057.jpeg "网站爬取页面规则管理")

爬取到的链接管理的页面
![爬取到的链接管理](https://gitee.com/uploads/images/2018/0126/104614_dffbc01f_126057.jpeg "爬取到的链接管理")

爬取的内容管理的页面
![爬取的内容管理](https://gitee.com/uploads/images/2018/0126/104702_20d51416_126057.jpeg "爬取的内容管理")


### 已完成
```
开发链接管理功能，要可以批量删除链接等
将文章和域名关联起来
开发提取网站的内容列表预览
开发网页管理，添加提取的网站
定义提取内容的链接的规则
定义提取内容的格式
根据提供的链接分析改链接下面的字连接，同时下载对应的页面到内存中。
```

### 待完成
```
分布式需要处理的地方
	重置爬虫
	爬取网页的地址的分配
```
